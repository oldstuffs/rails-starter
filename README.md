Project Readme
==============
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

```erb
<%= f.input :datetime, required: true, as: :datetime_picker %>
<%= f.input :date, required: true, as: :date_picker %>
<%= f.input :time, required: true, as: :time_picker %>
```

```erb
<%= f.input :name, wrapper: :vertical_input_group, hint: "Google" do %>
  <span class="input-group-addon">Prepend stuff</span>
  <%= f.input_field :name, class: "form-control" %>
  <span class="input-group-addon">Append stuff</span>
<% end %>
<%= f.input :name, wrapper: :horizontal_input_group, hint: "Google" do %>
  <span class="input-group-addon">Prepend stuff</span>
  <%= f.input_field :name, class: "form-control" %>
  <span class="input-group-addon">Append stuff</span>
<% end %>
```