module ApplicationHelper
  def render_page_title
    title = @page_title ? "#{@page_title} | Wilmot" : "Wilmot"
    content_tag(:title, title)
  end
  def resource_name
    :user
  end
  def resource
    @resource ||= User.new
  end
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
end
