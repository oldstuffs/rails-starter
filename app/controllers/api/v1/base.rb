module API
  module V1
    class Base < Grape::API
      version 'v1', using: :path, cascade: false
      
      post :token do
        email = params[:email]
        password = params[:password]

        if email.nil? or password.nil?
          error!("The request must contain the user email and password.", 400)
        end
        
        @user=User.find_by_email(email.downcase)
        
        if @user.nil?
          error!("Invalid email or passoword.", 401)
        end
        
        @user.ensure_authentication_token!
        
        if not @user.valid_password?(password)
          error!("Invalid email or passoword.", 401)
        else
          @user.save
          { token: @user.authentication_token }
        end
      end

      mount API::V1::Hello
      mount API::V1::Hi
    end
  end
end