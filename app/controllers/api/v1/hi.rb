module API
  module V1
    class Hi < Grape::API
      get :hi do
        {message: 'Hi,world API v1'}
      end
    end
  end
end