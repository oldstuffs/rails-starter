module API
  module V1
    class Hello < Grape::API
      before { authenticate! }
      
      get :hello do
        {message: 'Hello,world API v1', current_user_nil: current_user.nil?}
      end
    end
  end
end