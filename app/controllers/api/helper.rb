module API
  module Helper
    
    def warden
      env['warden']
    end
    
    def current_user
      warden.user || @user
    end
    
    def authenticated
      user_email = request.headers["X-Api-Email"].presence
      user_auth_token = request.headers["X-Api-Token"].presence
      user = user_email && User.find_by_email(user_email)

      if user && Devise.secure_compare(user.authentication_token, user_auth_token)
        @user = user
      else
        @user = nil
      end
    end
    
    def authenticate!
      @user = nil
      error!("401 Unauthorized", 401) unless authenticated
    end
    
    def god
      "This is god"
    end
  end
end